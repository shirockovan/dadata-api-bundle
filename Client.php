<?php

/* 
 *  Copyright 2016 Evgeniy Zhelyazkov <evgeniyzhelyazkov@gmail.com>.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace DadataApiBundle;

class Client
{
    
    private $client;
    
    public function __construct($apiKey, $baseUri, $contentType) {
        $this->client = new \Dadata_Client(array('api_key' => $apiKey, 'content_type' => $contentType, 'base_uri' => $baseUri));
    }
    
    public function getService($serviceName){
        $serviceClass = 'Dadata_Service_' . $serviceName;
        if(!class_exists($serviceClass, true)){
            return null;
        }
        return new $serviceClass($this->client);
    }
    
}